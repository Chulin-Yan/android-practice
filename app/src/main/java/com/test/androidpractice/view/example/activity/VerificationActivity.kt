package com.test.androidpractice.view.example.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.test.androidpractice.R
import com.test.androidpractice.base.BaseActivity
import com.test.androidpractice.databinding.ActivityLoginBinding
import com.test.androidpractice.databinding.ActivityVerificationBinding
import com.test.androidpractice.service.network.utils.Status
import com.test.androidpractice.viewmodel.LoginViewModel
import com.test.androidpractice.viewmodel.VerificationViewModel


/**
 * this is a example for activity with full general method
 * this also a good start for most project main page
 */
class VerificationActivity : BaseActivity() {
    private lateinit var binding: ActivityVerificationBinding
    private lateinit var viewModel: VerificationViewModel

    /**
     * activity starter
     */
    companion object {
        fun start(context: Context, phone: String) {
            val intent = Intent(context, VerificationActivity::class.java)
            intent.putExtra("phoneNumber",phone)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_verification)
        viewModel = ViewModelProvider(this).get(VerificationViewModel::class.java)
        viewModel.phoneNumber.value = intent.getStringExtra("phoneNumber")

        //data binding with view model
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        initView()
    }


    private fun callAPIsForVerification() {
        showLoading()
        viewModel.loadData(this, true).observe(this, Observer { it ->
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        hideLoading()
                        resource.data?.let {
                            Log.d("verify","success")
                            MainActivity.start(this,true)
                        }
                    }

                    Status.ERROR -> {
                        hideLoading()

                    }

                    Status.LOADING -> {

                    }
                }
            }
        })
    }


    private fun clickEvent() {
        //todo add click event
    }

    private fun initView() {
        binding.loginButton.setOnClickListener {
            if (TextUtils.isEmpty(viewModel.verificationCode.value)) {
                Toast.makeText(
                    this,
                    "Please input verification code",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            } else {
                callAPIsForVerification()
            }
        }

    }

    override fun onResume() {
        super.onResume()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


}