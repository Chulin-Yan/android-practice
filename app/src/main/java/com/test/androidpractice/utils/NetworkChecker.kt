package com.test.androidpractice.utils

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.provider.Settings
import androidx.annotation.RequiresApi
import com.test.androidpractice.SplashActivity

class NetworkChecker {


    /**
     * example set:
     *  //network
    if (!NetworkChecker.isConnective(this.applicationContext)) {
    NetworkChecker.askForInternet(this, "This App need Internet for use.", "Network ")
    }
    if (savedInstanceState != null) {
    val intent = Intent(this, SplashActivity::class.java)
    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    startActivity(intent)
    }
     */

    companion object {

        fun isConnective(context: Context): Boolean {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                return checkInternet(context)
            } else {
                val cm = context
                    .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager ?: return false

                val activeNetwork = cm.activeNetworkInfo
                return activeNetwork != null && activeNetwork.isConnectedOrConnecting
            }
        }

        @RequiresApi(Build.VERSION_CODES.N)
        fun checkInternet(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val nw = connectivityManager.activeNetwork ?: return false
            val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
            return when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        }

        fun askForInternet(context: Context, network_hint: String, title: String) {
            val dialog = AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(network_hint)
                .setPositiveButton("OK") { dialog, _ ->
                    dialog.dismiss()
                    val i = Intent(Settings.ACTION_WIFI_SETTINGS)
                    context.startActivity(i)
                }
                .setCancelable(false)
                .setNegativeButton("CANCEL") { _, _ ->
                    val intent = Intent(context, SplashActivity::class.java)
                    context.startActivity(intent)
                }
                .create()
            dialog.show()
        }
    }
}