package com.test.androidpractice.view.example.adapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.*
import com.test.androidpractice.R
import com.test.androidpractice.databinding.ItemCommitmentQuestionMultipleChoiceBinding
import com.test.androidpractice.databinding.ItemCommitmentQuestionSingleChoiceBinding
import com.test.androidpractice.databinding.ItemCommitmentQuestionTextBinding
import com.test.androidpractice.model.ChallengeCommitmentQuestionAnswerOptionData
import com.test.androidpractice.model.ChallengeCommitmentQuestionAnswerOptionDataWithValidation
import com.test.androidpractice.model.ChallengeCommitmentQuestionData
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList


class MyCommitmentAdapter(
    private val context: Context,
    private var displayList: ArrayList<ChallengeCommitmentQuestionData?>
) :
    RecyclerView.Adapter<MyCommitmentAdapter.ViewHolder>() {
    private var type: Int? = null
    private var answersToBePassed: ArrayList<String?>? = null

    /**
     * types
     */
    class ViewHolder(var dataBinding: ViewDataBinding) : RecyclerView.ViewHolder(dataBinding.root)
    companion object {
        const val TYPE_ONE = 0
        const val TYPE_TWO = 1
        const val TYPE_THREE = 2
    }

    /**
     * select interface
     */
    private var onSelectClickedOnSingleChoiceListener: OnSelectClickedOnSingleChoiceListener? = null

    fun setOnSelectClickedOnSingleChoiceListener(onSelectClickedListener: OnSelectClickedOnSingleChoiceListener?) {
        this.onSelectClickedOnSingleChoiceListener = onSelectClickedListener
    }

    interface OnSelectClickedOnSingleChoiceListener {
        fun onSelectClicked(item: Pair<Int,Int>)
    }

    private var onSelectClickedOnMultipleChoiceListener: OnSelectClickedOnMultipleChoiceListener? =
        null

    fun setOnSelectClickedOnMultipleChoiceListener(onSelectClickedListener: OnSelectClickedOnMultipleChoiceListener?) {
        this.onSelectClickedOnMultipleChoiceListener = onSelectClickedListener
    }

    interface OnSelectClickedOnMultipleChoiceListener {
        fun onSelectClicked(item: Pair<Int,Int>)
    }

    /**
     * init layout for different type
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        type = when (viewType) {
            TYPE_ONE -> TYPE_ONE
            TYPE_TWO -> TYPE_TWO
            else -> TYPE_THREE
        }
        return when (viewType) {
            TYPE_ONE -> ViewHolder(
                DataBindingUtil.inflate<ItemCommitmentQuestionMultipleChoiceBinding>(
                    inflater,
                    R.layout.item_commitment_question_multiple_choice,
                    parent,
                    false
                )
            )
            TYPE_TWO -> ViewHolder(
                DataBindingUtil.inflate<ItemCommitmentQuestionSingleChoiceBinding>(
                    inflater,
                    R.layout.item_commitment_question_single_choice,
                    parent,
                    false
                )
            )
            else -> ViewHolder(
                DataBindingUtil.inflate<ItemCommitmentQuestionTextBinding>(
                    inflater,
                    R.layout.item_commitment_question_text,
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemCount(): Int = displayList.size

    /**
     * select type, must have default type
     */
    override fun getItemViewType(position: Int): Int {
        return when (displayList[position]?.type) {
            "multiple_choice" -> {
                type = TYPE_ONE
                TYPE_ONE
            }
            "single_choice" -> {
                type = TYPE_TWO
                TYPE_TWO
            }
            else -> {
                type = TYPE_THREE
                TYPE_THREE
            }
        }
    }


    /**
     * edit action for item
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = displayList[position]
        item?.let {
            when (type) {
                TYPE_ONE -> {
                    val binding = holder.dataBinding as ItemCommitmentQuestionMultipleChoiceBinding
//                    var maxLines = when ((it.answer_options?.size)?.rem(2)) {
//                        0 -> {
//                            it.answer_options!!.size / 2
//                        }
//                        1 -> {
//                            (it.answer_options!!.size + 1) / 2
//                        }
//                        else -> {
//                            1
//                        }
//                    }
                    var maxLines = 2

                    binding.tvQuestionContent.text = displayList[position]?.title

                    var optionList: ArrayList<ChallengeCommitmentQuestionAnswerOptionDataWithValidation?> =
                        arrayListOf()

                    if (displayList[position]?.answer_options?.size != null) {
                        for (i in 0..(displayList[position]?.answer_options?.size!! - 1)) {
                            var status = false
                            if (displayList[position]?.answers?.size != null) {
                                for (i in 0..(displayList[position]?.answers?.size!! - 1)) {
                                    if (
                                        displayList[position]?.answers?.get(i) == position.toString()) {
                                        status = true
                                        break
                                    }
                                }
                            }


                            if (optionList != null) {
                                optionList.add(
                                    ChallengeCommitmentQuestionAnswerOptionDataWithValidation(
                                        displayList[position]?.answer_options?.get(i)?.id,
                                        displayList[position]?.answer_options?.get(i)?.text,
                                        status
                                    )
                                )
                            }

                        }
                    }
                    // set adapter for answer options
                    val optionAdapter =
                        CommitmentQuestionOptionAdapter(
                            context,
                            optionList,
                            "dark",
                        )
                    val gridLayoutManager = GridLayoutManager(context, maxLines)
                    gridLayoutManager.orientation = GridLayoutManager.VERTICAL

                    binding.rvOptions.apply {
                        layoutManager = gridLayoutManager
                        adapter = optionAdapter
                    }

                    optionAdapter.setOnSelectClickedListener(object :
                        CommitmentQuestionOptionAdapter.OnSelectClickedListener {


                        override fun onSelectClicked(item: Int) {
                            Log.d("type1",item.toString())
//                            optionList[item.toInt()]?.status =
//                                optionList[item.toInt()]?.status != true
//                            optionAdapter.notifyDataSetChanged()
                            binding.commitmentOptionItemMultipleChoice.setOnClickListener {
                                onSelectClickedOnMultipleChoiceListener?.onSelectClicked(
                                    Pair(
                                        position,
                                        item
                                    )
                                )
                            }
                            binding.commitmentOptionItemMultipleChoice.performClick()
                        }
                    })
                }
                TYPE_TWO -> {
                    val binding = holder.dataBinding as ItemCommitmentQuestionSingleChoiceBinding
                    binding.tvQuestionContent.text = displayList[position]?.title
                    val maxLines = when ((it.answer_options?.size)?.rem(2)) {
                        0 -> {
                            it.answer_options!!.size / 2
                        }
                        1 -> {
                            (it.answer_options!!.size + 1) / 2
                        }
                        else -> {
                            1
                        }
                    }
                    var optionList: ArrayList<ChallengeCommitmentQuestionAnswerOptionDataWithValidation?> =
                        arrayListOf()

                    if (displayList[position]?.answer_options?.size != null) {
                        for (i in 0..(displayList[position]?.answer_options?.size!! - 1)) {
                            var status = false
                            if (displayList[position]?.answers?.get(0) == position.toString()) {
                                status = true
                            }
                            if (optionList != null) {
                                optionList.add(
                                    ChallengeCommitmentQuestionAnswerOptionDataWithValidation(
                                        displayList[position]?.answer_options?.get(i)?.id,
                                        displayList[position]?.answer_options?.get(i)?.text,
                                        status
                                    )
                                )
                            }

                        }
                    }


                    // set adapter for answer options
                    answersToBePassed = displayList[position]?.answers
                    var optionAdapter =
                        CommitmentQuestionOptionAdapter(
                            context, optionList, "light",
                        )
                    var flexBoxLayoutManager = FlexboxLayoutManager(context)
                    flexBoxLayoutManager.flexDirection = FlexDirection.ROW
                    flexBoxLayoutManager.flexWrap = FlexWrap.WRAP
                    flexBoxLayoutManager.justifyContent = JustifyContent.FLEX_START
                    flexBoxLayoutManager.alignItems = AlignItems.CENTER
                    flexBoxLayoutManager.maxLine = maxLines
                    binding.rvOptions.apply {
                        layoutManager = flexBoxLayoutManager
                        adapter = optionAdapter
                    }
                    // set onSelected listener
                    optionAdapter.setOnSelectClickedListener(object :
                        CommitmentQuestionOptionAdapter.OnSelectClickedListener {
                        override fun onSelectClicked(item: Int) {
                            Log.d("position", "list size " + optionList.size.toString())
                            for (i in 0..optionList.size - 1) {
                                Log.d("position", item.toString())
                                optionList[i]?.status = i == item
                                Log.d("position", optionList[i]?.status.toString())
                            }
                            optionAdapter.notifyDataSetChanged()
                            binding.commitmentOptionItemSingleChoice.setOnClickListener {
                                onSelectClickedOnSingleChoiceListener?.onSelectClicked(
                                    Pair(
                                        position,
                                        item
                                    )
                                )
                            }
                            binding.commitmentOptionItemSingleChoice.performClick()

                        }

                    })
                }
                else -> {
                    val binding = holder.dataBinding as ItemCommitmentQuestionTextBinding
                    binding.tvQuestionContent.text = displayList[position]?.title
                    var timer = Timer()
                    val delay: Long = 3000

                    // automatically post user's input
                    binding.etUserInput.addTextChangedListener(object : TextWatcher {
                        override fun beforeTextChanged(
                            s: CharSequence, start: Int, count: Int,
                            after: Int
                        ) {
                        }

                        override fun onTextChanged(
                            s: CharSequence, start: Int, before: Int,
                            count: Int
                        ) {
                            if (timer != null) timer.cancel()
                        }

                        override fun afterTextChanged(s: Editable) {
                            //avoid triggering event when text is too short
                            if (s.length >= 3) {
                                timer = Timer()
                                timer.schedule(object : TimerTask() {
                                    override fun run() {
                                    }
                                }, delay)
                            }
                        }
                    })
                }
            }
        }
    }
}