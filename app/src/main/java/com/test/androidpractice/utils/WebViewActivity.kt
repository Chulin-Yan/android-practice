package com.test.androidpractice.utils

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import com.test.androidpractice.R
import com.test.androidpractice.base.BaseActivity
import com.test.androidpractice.base.Global.Companion.URL_DATA
import com.test.androidpractice.databinding.ActivityWebViewBinding

class WebViewActivity : BaseActivity() {
    private lateinit var binding: ActivityWebViewBinding
    private var url: String? = null

    companion object {
        fun start(context: Context, url: String) {
            val intent = Intent(context, WebViewActivity::class.java)
            intent.putExtra(URL_DATA, url)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_web_view)
        url = intent.getStringExtra(URL_DATA)
        if (url != null) {
            Log.d("check", url!!)
            initWebView()
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        val webViewClient = object : WebViewClient() {
            //            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap) {
//                super.onPageStarted(view, url, favicon)
//            }
//
//            override fun onPageFinished(view: WebView, url: String) {
//                super.onPageFinished(view, url)
//            }
            override fun onReceivedError(
                view: WebView,
                request: WebResourceRequest,
                error: WebResourceError
            ) {
                super.onReceivedError(view, request, error)
                view.loadUrl("about:blank")
                showError(error.description.toString())
            }

            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                if (request?.url.toString().startsWith("tel:")) {
                    val intent = Intent(
                        Intent.ACTION_DIAL,
                        Uri.parse(request?.url.toString())
                    )
                    startActivity(intent)
                } else {
                    view?.loadUrl(request?.url.toString());
                }
                return true
            }
        }
        binding.webView.settings.javaScriptEnabled = true
        binding.webView.isHorizontalScrollBarEnabled = false
        binding.webView.isVerticalScrollBarEnabled = true
        binding.webView.isScrollbarFadingEnabled = false
        binding.webView.webViewClient = webViewClient
        binding.webView.loadUrl(url!!)
    }

    private fun showError(error: String) {
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle("Error")
        dialog.setMessage(error)
        dialog.setPositiveButton("Ok") { _, _ ->
            onBackPressed()
        }
        dialog.show()
    }

    override fun onBackPressed() {
        if (binding.webView.canGoBack()) {
            binding.webView.goBack()
        } else {
            super.onBackPressed()
        }
    }
}