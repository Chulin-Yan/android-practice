package com.test.androidpractice.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.test.androidpractice.base.BaseViewModel
import com.test.androidpractice.service.network.RetrofitBuilder
import com.test.androidpractice.service.network.utils.Resource

import kotlinx.coroutines.Dispatchers

class MyCommitmentViewModel : BaseViewModel() {
    private val repository = RetrofitBuilder.apiService

    fun loadData(context: Context, displayToast: Boolean) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = repository.getChallengeCommitmentQuestion().body()))
        } catch (exception: Exception) {
            printErrorMessage(context, exception.message ?: defaultError, displayToast)
            emit(Resource.error(data = null, message = exception.message ?: defaultError))
        }
    }
}