package com.test.androidpractice.view.example.activity

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.test.androidpractice.R
import com.test.androidpractice.SplashActivity
import com.test.androidpractice.base.BaseActivity
import com.test.androidpractice.databinding.ActivityMainBinding
import com.test.androidpractice.databinding.ActivityMyCommitmentBinding
import com.test.androidpractice.model.BreakfastItem
import com.test.androidpractice.model.ChallengeCommitmentQuestion
import com.test.androidpractice.model.ChallengeCommitmentQuestionData
import com.test.androidpractice.service.network.utils.Status
import com.test.androidpractice.service.network.utils.callAPI
import com.test.androidpractice.utils.ReadAssetUtil
import com.test.androidpractice.view.example.adapter.MainAdapter
import com.test.androidpractice.view.example.adapter.MyCommitmentAdapter
import com.test.androidpractice.viewmodel.MainViewModel
import com.test.androidpractice.viewmodel.MyCommitmentViewModel
import kotlinx.android.synthetic.main.activity_my_commitment.view.*


/**
 * this is a example for activity with full general method
 * this also a good start for most project main page
 */
class MyCommitmentActivity : BaseActivity() {
    private lateinit var binding: ActivityMyCommitmentBinding
    private lateinit var viewModel: MyCommitmentViewModel
    private lateinit var mAdapter: MyCommitmentAdapter
    private var displayList: ArrayList<ChallengeCommitmentQuestionData?> = arrayListOf()

    /**
     * activity starter
     */
    companion object {
        fun start(context: Context) {
            val intent = Intent(context, MyCommitmentActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // set status bar color
        val window: Window = window
        window.setStatusBarColor(Color.parseColor("#2A303F"))
        if (Build.VERSION.SDK_INT < 30) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        } else {
            window.setDecorFitsSystemWindows(false)
//            val controller = getWindow().insetsController
//            controller?.setSystemBarsAppearance(
//            )
        }
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_commitment)
        viewModel = ViewModelProvider(this).get(MyCommitmentViewModel::class.java)

        //data binding with view model
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        //set adapter
        mAdapter = MyCommitmentAdapter(this, displayList)
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        binding.rvQuestions.apply {
            layoutManager = linearLayoutManager
            adapter = mAdapter
        }
        //general function
        //initView()
        // clickEvent()
        callAPIsForChallengeQuestions()
        //simple call
        // callAPI()

    }


    private fun callAPIsForChallengeQuestions() {
        //       updateBreakfastItem(readTestInput())
        showLoading()
        viewModel.loadData(this, true).observe(this, Observer { it ->
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        hideLoading()
                        resource.data?.let {
                            updateChallengeQuestions(it)

                        }
                    }

                    Status.ERROR -> {
                        hideLoading()
                    }

                    Status.LOADING -> {

                    }
                }
            }
        })
    }


    private fun updateChallengeQuestions(questionPack: ChallengeCommitmentQuestion) {
        //todo update user
        displayList.clear()
        questionPack.black_area?.let { displayList.addAll(it) }
        questionPack.white_area?.let { displayList.addAll(it) }
        mAdapter.notifyDataSetChanged()
        binding.btnContinue.visibility = View.VISIBLE
    }

    private fun updateAnswers(indexes: Pair<Int, Int>) {
        if (displayList[indexes.first]?.answers != null) {
            when (displayList[indexes.first]?.type) {
                "multiple_choice" -> {
                    if (displayList[indexes.first]?.answers?.contains((indexes.second+1).toString()) == true) {
                        displayList[indexes.first]?.answers?.remove((indexes.second+1).toString())
                    }else{
                        displayList[indexes.first]?.answers?.add((indexes.second+1).toString())
                    }
                }
                "single_choice" -> {
                    if (displayList[indexes.first]?.answers?.contains((indexes.second+1).toString()) == true) {
                    }else{
                        displayList[indexes.first]?.answers?.clear()
                        displayList[indexes.first]?.answers?.add((indexes.second+1).toString())
                    }
                }
                else -> {

                }
            }
        } else {
            displayList[indexes.first]?.answers = arrayListOf((indexes.second+1).toString())
        }
    }

    private fun clickEvent() {
        mAdapter.setOnSelectClickedOnSingleChoiceListener(object :
            MyCommitmentAdapter.OnSelectClickedOnSingleChoiceListener {
            override fun onSelectClicked(item: Pair<Int, Int>) {
                var questionItemPosition = item.first.toInt()
                var optionItemPosition = item.second.toInt()
//            if(displayList[questionItemPosition]?.answers !=null){
//                displayList[questionItemPosition]?.answers?.set(0,
//                    (optionItemPosition + 1).toString()
//                )
//                mAdapter.notifyDataSetChanged()
//            }else{
//                var newAnswers = ArrayList<String?>()
//                newAnswers.add((optionItemPosition+1).toString())
//                var newDisplayListItem = ChallengeCommitmentQuestionData(
//                    displayList[questionItemPosition]?.id,
//                    displayList[questionItemPosition]?.title,
//                    displayList[questionItemPosition]?.type,
//                    displayList[questionItemPosition]?.answer_options,
//                    newAnswers,
//                    displayList[questionItemPosition]?.text_answer
//                )
//                displayList[questionItemPosition] = newDisplayListItem
//                mAdapter.notifyDataSetChanged()
//            }
            }
        })

        mAdapter.setOnSelectClickedOnMultipleChoiceListener(object :
            MyCommitmentAdapter.OnSelectClickedOnMultipleChoiceListener {
            override fun onSelectClicked(item: Pair<Int, Int>) {


                Log.d("click", "multiple" + item)
                var questionItemPosition = item.first.toInt()
                var optionItemPosition = item.second.toInt()
                if (displayList[questionItemPosition]?.answers != null) {
                    if (displayList[questionItemPosition]?.answers!!.contains((optionItemPosition + 1).toString())) {
                        displayList[questionItemPosition]?.answers!!.remove((optionItemPosition + 1).toString())
                    } else {
                        displayList[questionItemPosition]?.answers!!.add((optionItemPosition + 1).toString())
                    }
                    mAdapter.notifyDataSetChanged()
                } else {
                    var newAnswers = ArrayList<String?>()
                    newAnswers.add((optionItemPosition + 1).toString())
                    var newDisplayListItem = ChallengeCommitmentQuestionData(
                        displayList[questionItemPosition]?.id,
                        displayList[questionItemPosition]?.title,
                        displayList[questionItemPosition]?.type,
                        displayList[questionItemPosition]?.optional,
                        displayList[questionItemPosition]?.answer_options,
                        newAnswers,
                        displayList[questionItemPosition]?.answer_text
                    )
                    displayList[questionItemPosition] = newDisplayListItem
                    mAdapter.notifyDataSetChanged()
                }
            }
        })

        binding.toolbar.iv_return_to_previous.setOnClickListener {
            SplashActivity.start(this)
        }
    }
//    private fun jumpToDetail(){
//        BreakfastDetailActivity.start(this)
//    }
//    private fun initView() {
//        //todo init view
//        //get/set live data programmatically
//        val anotherName = "John Doe"
//        viewModel.currentName.value = anotherName
//        //observe live data programmatically
//        viewModel.currentName.observe(this, {
//            //binding.tvText2.text = it
//        })
//    }

    override fun onResume() {
        super.onResume()

    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


    fun checkReadyToContinue(): Boolean {
        var enableFlag = true
        displayList.forEach {
            if (it != null) {
                if (it.optional != true) {
                    when(it.type){
                        "text" -> {
                            if (it.answer_text.isNullOrEmpty()) {
                                enableFlag = false
                                return enableFlag
                            }
                        }
                        else ->{
                            if (it.answers.isNullOrEmpty()) {
                                enableFlag = false
                                return enableFlag
                            }
                        }
                    }
                }
            }
        }
        return enableFlag
    }
}