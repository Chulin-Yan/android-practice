package com.test.androidpractice.service.network.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}