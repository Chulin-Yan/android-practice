package com.test.androidpractice.view.example.activity

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.Window
import androidx.databinding.DataBindingUtil
import com.test.androidpractice.R
import com.test.androidpractice.base.BaseActivity
import com.test.androidpractice.databinding.ActivityBreakfastDetailBinding
import com.test.androidpractice.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_breakfast_detail.*


/**
 * Example Activity
 * Please copy me when you need create new Activity
 */
class BreakfastDetailActivity : BaseActivity() {
    private lateinit var binding: ActivityBreakfastDetailBinding

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, BreakfastDetailActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_breakfast_detail)

        //general function
        initView()
        clickEvent()
    }

    private fun setStatusBar() {
        val window: Window = this.window
        //full screen or light status bar
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R) {
            window.setDecorFitsSystemWindows(false)
        } else {
            window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        window.statusBarColor = Color.WHITE
    }

    private fun clickEvent() {
        //todo add click event

        binding.tvBackward.setOnClickListener {
            MainActivity.start(this, false)
        }
    }

    private fun initView() {
        //todo init view
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}