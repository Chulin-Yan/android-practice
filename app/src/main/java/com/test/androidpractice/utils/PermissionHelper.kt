package com.test.androidpractice.utils

import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.app.ActivityCompat.shouldShowRequestPermissionRationale
import androidx.core.content.ContextCompat


//accessNotificationPolicy = Manifest.permission.ACCESS_NOTIFICATION_POLICY

fun checkPermission(
    activity: Activity,
    requestCode: Int,
    accessNotificationPolicy: String
): Boolean {
    when {
        ContextCompat.checkSelfPermission(
            activity,
            accessNotificationPolicy
        ) == PackageManager.PERMISSION_GRANTED -> {
            // You can use the API that requires the permission.
            return true
        }
        shouldShowRequestPermissionRationale(
            activity,
            accessNotificationPolicy
        ) -> {
            // In an educational UI, explain to the user why your app requires this
            // permission for a specific feature to behave as expected. In this UI,
            // include a "cancel" or "no thanks" button that allows the user to
            // continue using your app without granting the permission.
//                showInContextUI(...)
        }
        else -> {
            // You can directly ask for the permission.
            requestPermissions(
                activity,
                arrayOf(accessNotificationPolicy),
                requestCode
            )
            return false
        }
    }
    return false
}
