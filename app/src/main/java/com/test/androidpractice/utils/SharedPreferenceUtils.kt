package com.test.androidpractice.utils

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson

class SharedPreferenceUtils {

    companion object{
        private lateinit var editor: SharedPreferences.Editor
        private lateinit var sharedPreferences: SharedPreferences
        private val gson = Gson()

        fun initSP(context: Context,fileName:String) {
            sharedPreferences = context.getSharedPreferences(
                fileName,
                Context.MODE_PRIVATE
            )
            editor = sharedPreferences.edit()
        }

        /**
         * save
         */
        fun put(key: String?, data: Any) {
            editor.putString(key, toJson(data))
            try {
                editor.apply()
            } catch (unused: AbstractMethodError) {
                // The app injected its own pre-Gingerbread
                // SharedPreferences.Editor implementation without
                // an apply method.
                editor.commit()
            }
        }

        fun toJson(data: Any?): String? {
            var json: String? = null
            json = gson.toJson(data)
            return json
        }

        fun <T> parse(json: String?, cls: Class<T>?): T? {
            return gson.fromJson(json, cls)
        }

        /**
         * load
         */
        fun <T> getSharedPreference(key: String?, cls: Class<T>?): T? {
            return parse(sharedPreferences.getString(key, null), cls)
        }

        /**
         * remove
         */
        fun remove(key: String?) {
            editor.remove(key)
            editor.commit()
        }

        /**
         * clear
         */
        fun clear() {
            editor.clear()
            editor.commit()
        }

        /**
         * search a single key
         */
        fun contain(key: String?): Boolean {
            return sharedPreferences.contains(key)
        }


        /**
         * get all
         */
        val all: Map<String, *>
            get() = sharedPreferences.all

    }

}