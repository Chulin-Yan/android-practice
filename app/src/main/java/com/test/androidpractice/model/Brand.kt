package com.test.androidpractice.model

import com.google.gson.annotations.SerializedName

class Brand (
    @SerializedName("name")
    var name: String?,
    @SerializedName("left_color")
    var leftColor: String?,
    @SerializedName("right_color")
    var rightColor: String?,
    @SerializedName("logo_image")
    var logoImage: String?,
    @SerializedName("tag_image")
    var tagImage: String?,
    @SerializedName("banner_title")
    var bannerTitle: String?,
)