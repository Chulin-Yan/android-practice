package com.test.androidpractice.model

import com.google.gson.annotations.SerializedName

data class UserOrigin(
    @SerializedName("id")
    var id:Int?,
    @SerializedName("name")
    val name:String?
)