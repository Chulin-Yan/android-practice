package com.test.androidpractice.model

import com.google.gson.annotations.SerializedName

data class ChallengeCommitmentQuestionData(
    @SerializedName("id")
    var id: String?,
    @SerializedName("title")
    var title: String?,
    @SerializedName("type")
    var type: String?,
    @SerializedName("optional")
    var optional: Boolean?,
    @SerializedName("answer_options")
    var answer_options: ArrayList<ChallengeCommitmentQuestionAnswerOptionData?>?,
    @SerializedName("answers")
    var answers: ArrayList<String?>?,
    @SerializedName("answer_text")
    var answer_text: String?
)