package com.test.androidpractice.base

import android.content.Context
import android.os.Looper
import android.widget.Toast
import androidx.lifecycle.ViewModel
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.Response

abstract class BaseViewModel : ViewModel() {

    var defaultError="Error Occurred!"

    /**
     * Utils
     * multipart data creator
     */
    private fun createPartFromString(param: String?): RequestBody? {
        return if (param != null) {
            val mediaType = "multipart/form-data".toMediaType()
            param.toString().toRequestBody(mediaType)
        } else {
            null
        }
    }

    /**
     * display a toast with error message from server
     */
    fun printErrorMessage(context: Context, message:String,displayToast:Boolean){
        if(displayToast) {
            Looper.prepare()
            if (message.contains("Failed to connect")) {
                Toast.makeText(context, "Loading Data Fail", Toast.LENGTH_LONG
                ).show()
            } else {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show()
            }
            Looper.loop()
        }
    }

    /**
     * extract error body
     */
    fun extractErrorBody(response: Response<*>):String {
        var m = defaultError
        response.errorBody()?.let {
            val jObjError = JSONObject(it.string())
            jObjError.optJSONArray("errors")?.let {
                m = jObjError.optJSONArray("errors")!![0].toString()
            }
        }
        return m
    }
}
