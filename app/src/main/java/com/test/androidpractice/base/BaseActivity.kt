package com.test.androidpractice.base

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.facebook.appevents.AppEventsLogger
import com.test.androidpractice.R
import com.test.androidpractice.SplashActivity
import com.test.androidpractice.utils.NetworkChecker

open class BaseActivity : AppCompatActivity(), BaseView {
    private lateinit var progressLoading: Dialog
    private val progressBackground = ColorDrawable(Color.TRANSPARENT)


    /**
     * loading display variables
     */
    private lateinit var loadingHandler:Handler //show/hide loading
    private lateinit var r : Thread //loading thread
    private var stop=false //safety control

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        progressLoading = Dialog(this)
        loadingHandler= Handler(Looper.getMainLooper())
        r=object : Thread() {
            override fun run() {
                loadingHandler.postDelayed({
                    if(!stop) {
                        progressLoading.show()
                    }else{
                        stop=false
                    }
                }, 800)
            }
        }

        //network
        if (!NetworkChecker.isConnective(this.applicationContext)) {
            NetworkChecker.askForInternet(this, "This App need Internet for use.", "Network ")
        }

        if (savedInstanceState != null) {
            val intent = Intent(this, SplashActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }

        // init progress loading
        createProgressLoading()


        /***
         * status bar setting
         * these method are deprecated in android R (30)
         * but they still able to use in 24-29
         */
//        val window = window
//        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
//        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
//        window.statusBarColor = Color.TRANSPARENT
//        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    }

    protected open fun createProgressLoading() {
        progressLoading.setCancelable(false)
        val window: Window? = progressLoading.window
        window?.setBackgroundDrawable(progressBackground)
        progressLoading.setContentView(R.layout.dialog_default_progress)
    }

    override fun showLoading() {
        if(!stop) {
            r.start()
        }
    }

    override fun hideLoading() {
        stop=true
        progressLoading.dismiss()
        r.interrupt()
    }

    override fun onResume() {
        if (!NetworkChecker.isConnective(this.applicationContext)) {
            NetworkChecker.askForInternet(this, "This App need Internet for use.", "Network ")
        }
        super.onResume()
        stop=false
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    fun facebookLogSent(log: String) {
        val appEventsLogger = AppEventsLogger.newLogger(this)
        appEventsLogger.logEvent(log)
    }

    override fun onPause() {
        super.onPause()
        stop=true
        r.interrupt()
    }

    override fun onDestroy() {
        if(r.isAlive) {
            stop = true
            r.interrupt()
        }
        super.onDestroy()
    }
}