package com.test.androidpractice.model

import com.google.gson.annotations.SerializedName

data class BreakfastItem(
    @SerializedName("name")
    var description: String?,
    @SerializedName("image")
    var imageName: String?,
    @SerializedName("brand")
    var brand: Brand?,
    @SerializedName("favorite")
    var favorite: Boolean?,
    @SerializedName("prep_time")
    var prep_time: String?,
    @SerializedName("serving_size")
    var serving_size: Int?,
    @SerializedName("calorie")
    var calorie: Int?
    )