package com.test.androidpractice.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.test.androidpractice.base.BaseViewModel
import com.test.androidpractice.service.network.RetrofitBuilder
import com.test.androidpractice.service.network.utils.Resource
import kotlinx.coroutines.Dispatchers

class VerificationViewModel : BaseViewModel() {
    private var repository = RetrofitBuilder.apiService
    var phoneNumber = MutableLiveData<String>()
    var verificationCode = MutableLiveData<String>()

    fun loadData(context: Context, displayToast: Boolean) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = phoneNumber.value?.let { verificationCode.value?.let { it1 ->
                repository.signIn(it,
                    it1
                ).body()
            } }))
        } catch (exception: Exception) {
            printErrorMessage(context, exception.message ?: defaultError, displayToast)
            emit(Resource.error(data = null, message = exception.message ?: defaultError))
        }
    }
}