package com.test.androidpractice.service.network.interceptors

import android.util.Log
import com.test.androidpractice.AccountManager
import com.test.androidpractice.model.User
import com.test.androidpractice.utils.SharedPreferenceUtils
import okhttp3.Interceptor
import okhttp3.Response

class AuthorizationInterceptor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
//        request = request.newBuilder()
//            .header("X-API-IDENTITY", "pixelforce")
//            .header("X-API-TOKEN", "")
//            .header("X-API-UUID", "")
//            .build()
        //  token = AccountManager.getToken()
        var token: User.Token? =
            SharedPreferenceUtils.getSharedPreference("token_info", User.Token::class.java)

        if (token != null) {
//            if (token.accessToken != null) {
            Log.d("auth", token.accessToken)
            Log.d("auth", token.tokenType)
            Log.d("auth", token.client)
            Log.d("auth", token.expiry)
            Log.d("auth", token.uid)
            request = request.newBuilder()
                .header("access-token", token.accessToken)
                .header("token-type", token.tokenType)
                .header("client", token.client)
                .header("expiry", token.expiry)
                .header("uid", token.uid)
                .header("X-API-IDENTITY", "pixelforce")
                .header("X-API-TOKEN", "jKhBPsvoSCXBb9H0qTnXQ1XavDrOsLCsBpAvD4QRm20=")
                .header("X-API-UUID", "8dsf8h9dfhun3")
                .header("X-API-DEVICE-TOKEN", "")
                .header("X-PLATFORM", "IOS")
                .header("X-APP-VERSION", "2.0.0")
                .build()
        } else {
            request = request.newBuilder()
                .header("X-API-IDENTITY", "pixelforce")
                .header("X-API-TOKEN", "jKhBPsvoSCXBb9H0qTnXQ1XavDrOsLCsBpAvD4QRm20=")
                .header("X-API-UUID", "8dsf8h9dfhun3")
                .header("X-API-DEVICE-TOKEN", "")
                .header("X-PLATFORM", "IOS")
                .header("X-APP-VERSION", "2.0.0")
                .build()
        }
        // }
        return chain.proceed(request)
    }


}