package com.test.androidpractice.base

interface BaseView {
    fun showLoading()
    fun hideLoading()
}