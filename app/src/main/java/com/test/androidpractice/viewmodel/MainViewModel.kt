package com.test.androidpractice.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.test.androidpractice.base.BaseViewModel
import com.test.androidpractice.service.network.RetrofitBuilder
import com.test.androidpractice.service.network.utils.Resource
import kotlinx.coroutines.Dispatchers

class MainViewModel : BaseViewModel() {
    private var repository = RetrofitBuilder.apiService

    val currentName: MutableLiveData<String> by lazy { MutableLiveData<String>() }

    fun loadData(context: Context, displayToast: Boolean) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = repository.getRecipes("","","1613016571","1","performance").body()))
        } catch (exception: Exception) {
            printErrorMessage(context, exception.message ?: defaultError, displayToast)
            emit(Resource.error(data = null, message = exception.message ?: defaultError))
        }
    }
}