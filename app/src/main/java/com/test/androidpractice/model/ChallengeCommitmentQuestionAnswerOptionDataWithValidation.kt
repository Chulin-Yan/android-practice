package com.test.androidpractice.model

import com.google.gson.annotations.SerializedName

data class ChallengeCommitmentQuestionAnswerOptionDataWithValidation(
    @SerializedName("id")
    var id: String?,
    @SerializedName("text")
    var text: String?,
    @SerializedName("status")
    var status: Boolean?,
)

