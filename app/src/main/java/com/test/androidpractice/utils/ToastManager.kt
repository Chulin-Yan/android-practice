package com.test.androidpractice.utils

import android.annotation.SuppressLint
import android.content.Context
import android.text.TextUtils
import android.widget.Toast

private var mToast: Toast? = null

@SuppressLint("ShowToast")
fun showToast(context: Context?, toastInfo: String?) {
    if (null == context || TextUtils.isEmpty(toastInfo)) {
        return
    }
    mToast = if (mToast==null) {
        Toast.makeText(context, toastInfo, Toast.LENGTH_SHORT)
    } else {
        hideToast()
        Toast.makeText(context, toastInfo, Toast.LENGTH_SHORT)
    }
    mToast!!.show()
}

fun hideToast() {
    mToast!!.cancel()
}

