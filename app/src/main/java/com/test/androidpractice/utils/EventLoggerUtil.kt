package com.test.androidpractice.utils

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import com.facebook.appevents.AppEventsLogger
import com.google.firebase.analytics.FirebaseAnalytics
import com.test.androidpractice.base.Global.Companion.CHECK
import java.util.*

class EventLoggerUtil {

    @SuppressLint("LogNotTimber")
    fun firebaseEvent(context: Context, id: String, codeName: String?, surveyID: String?) {
        val bundle = Bundle()
        codeName?.let { bundle.putString("code_name", it) }
        surveyID?.let { bundle.putString("survey_id", it) }
        bundle.putString("created_at", dateToTimestamp().toString())
        val firebaseAnalytics = FirebaseAnalytics.getInstance(context)
        firebaseAnalytics.logEvent(id, bundle)
        Log.d(CHECK, "Firebase:Sending event log -- id: $id, code name: $codeName, survey id: $surveyID, time: ${dateToTimestamp()}")
    }

    @SuppressLint("LogNotTimber")
    fun facebookEvent(context: Context, id: String, codeName: String?, surveyID: String?) {
        val logger = AppEventsLogger.newLogger(context)
        val bundle = Bundle()
        codeName?.let { bundle.putString("code_name", it) }
        surveyID?.let { bundle.putString("survey_id", it) }
        bundle.putString("created_at", dateToTimestamp().toString())
        logger.logEvent(id, bundle)
        Log.d(CHECK, "Facebook:Sending event log -- id: $id, code name: $codeName, survey id: $surveyID, time: ${dateToTimestamp()}")
    }

    private fun dateToTimestamp(): Long {
        val cal = Calendar.getInstance()
        return cal.timeInMillis / 1000L
    }
}