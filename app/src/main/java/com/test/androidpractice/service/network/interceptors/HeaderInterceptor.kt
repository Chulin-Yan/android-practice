package com.test.androidpractice.service.network.interceptors


import android.util.Log
import com.test.androidpractice.AccountManager
import com.test.androidpractice.model.User.Token
import com.test.androidpractice.utils.SharedPreferenceUtils
import okhttp3.Interceptor
import okhttp3.Response

class HeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val token = Token()
        val response = chain.proceed(chain.request())
        if (response.request.url.encodedPath == "/api/v1/auth/sign_in" && response.isSuccessful) {
            Log.d("access-token","before")
            response.header("access-token")?.let { Log.d("access-token", it) }
            response.header("token-type")?.let { Log.d("access-token", it) }
            response.header("client")?.let { Log.d("access-token", it) }
            response.header("expiry")?.let { Log.d("access-token", it) }
            response.header("uid")?.let { Log.d("access-token", it) }

            token.accessToken = response.header("access-token")
            token.tokenType = response.header("token-type")
            token.client = response.header("client")
            token.expiry = response.header("expiry")
            token.uid = response.header("uid")

//           AccountManager.userToken = token
//            AccountManager.saveToken(token)

            SharedPreferenceUtils.put("token_info",token)
        }
        return chain.proceed(chain.request())
    }
}