package com.test.androidpractice.view.example.adapter

import android.content.Context
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.test.androidpractice.R
import com.test.androidpractice.databinding.ItemCommitmentAnswerOptionDarkBinding
import com.test.androidpractice.databinding.ItemCommitmentAnswerOptionLightBinding
import com.test.androidpractice.model.ChallengeCommitmentQuestionAnswerOptionData
import com.test.androidpractice.model.ChallengeCommitmentQuestionAnswerOptionDataWithValidation
import java.lang.Exception


class CommitmentQuestionOptionAdapter(
    private val context: Context,
    private val displayList: ArrayList<ChallengeCommitmentQuestionAnswerOptionDataWithValidation?>,
    private val mode: String,
) :
    RecyclerView.Adapter<CommitmentQuestionOptionAdapter.ViewHolder>() {
    private var type: Int? = null

    /**
     * types
     */
    class ViewHolder(var dataBinding: ViewDataBinding) : RecyclerView.ViewHolder(dataBinding.root)
    companion object {
        const val TYPE_ONE = 0
        const val TYPE_TWO = 1
    }

    /**
     * select interface
     */
    private var onSelectClickedListener: OnSelectClickedListener? = null

    fun setOnSelectClickedListener(onSelectClickedListener: OnSelectClickedListener?) {
        this.onSelectClickedListener = onSelectClickedListener
    }

    interface OnSelectClickedListener {
        fun onSelectClicked(item: Int)
    }

    /**
     * init layout for different type
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        type = when (viewType) {
            TYPE_ONE -> TYPE_ONE
            else -> TYPE_TWO
        }
        return when (viewType) {
            TYPE_ONE -> ViewHolder(
                DataBindingUtil.inflate<ItemCommitmentAnswerOptionDarkBinding>(
                    inflater,
                    R.layout.item_commitment_answer_option_dark,
                    parent,
                    false
                )
            )
            else -> ViewHolder(
                DataBindingUtil.inflate<ItemCommitmentAnswerOptionLightBinding>(
                    inflater,
                    R.layout.item_commitment_answer_option_light,
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemCount(): Int = displayList.size

    /**
     * select type, must have default type
     */
    override fun getItemViewType(position: Int): Int {
        return when (mode) {
            "dark" -> {
                type = TYPE_ONE
                TYPE_ONE
            }
            else -> {
                type = TYPE_TWO
                TYPE_TWO
            }
        }
    }

    /**
     * edit action for item
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = displayList[position]
        val maxStringLength = 30
        var originalTexLength: Int
        var status = displayList[position]?.status

        item?.let {
            if (it?.text != null) {
                originalTexLength = it.text!!.length
            } else {
                originalTexLength = 0
            }

            when (type) {
                TYPE_ONE -> {
                    val binding = holder.dataBinding as ItemCommitmentAnswerOptionDarkBinding
                    var stringToBeAdded: String = it?.text as String
                    if (stringToBeAdded.length < maxStringLength) {
                        for (i in 1..(maxStringLength - originalTexLength)) {
                            stringToBeAdded = stringToBeAdded.plus(" ")
                        }
                    }
                    binding.tvOptionText.text = stringToBeAdded
                    if(status == true){
                        binding.ivOptionImage.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_option_ticked))
                    }
                    binding.commitmentAnswerOptionDark.setOnClickListener{
                        if(status == true){
                            binding.ivOptionImage.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_option_deselected))
                            status = false
                        }else{
                            binding.ivOptionImage.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_option_ticked))
                            status = true
                        }
                        onSelectClickedListener?.onSelectClicked(position)
                    }
                }
                else -> {
                    val binding = holder.dataBinding as ItemCommitmentAnswerOptionLightBinding
                    binding.tvOptionText.text = displayList?.get(position)?.text
                    if(status == true){
                        binding.ivOptionImage.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_option_selected))
                    }else{
                        binding.ivOptionImage.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_option_deselected))
                    }
                   //binding.ivOptionImage.tag = Pair(indexOfQuestion,position)

                    Log.d("ivOption",binding.ivOptionImage.id.toString())
                    binding.ivOptionImage.setOnClickListener{
                        onSelectClickedListener?.onSelectClicked(position)
                    }
                }
            }

        }
    }
}