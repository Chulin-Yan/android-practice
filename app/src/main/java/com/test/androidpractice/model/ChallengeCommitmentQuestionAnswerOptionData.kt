package com.test.androidpractice.model

import com.google.gson.annotations.SerializedName

data class ChallengeCommitmentQuestionAnswerOptionData(
    @SerializedName("id")
    var id: String?,
    @SerializedName("text")
    var text: String?,
)

