package com.test.androidpractice.service.network.interceptors

import android.util.Log
import com.orhanobut.logger.Logger
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody


class LoggingInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val t1 = System.nanoTime()
        Logger.i("Sending request %s on %s%n%s%n%s", request.url, chain.connection(), request.headers,request.body)

        val response = chain.proceed(request)
        val responseBodyString = response.body!!.string()
        val t2 = System.nanoTime()
        Log.d("check: ", response.handshake.toString())
        Logger.d("Received response for %s in %.1fms%nResponse Code: %s%n%s", response.request.url, (t2 - t1) / 1e6, response.code,response.headers)
        Logger.json(responseBodyString)

        return response.newBuilder().body(ResponseBody.create(response.body!!.contentType(), responseBodyString)).build()
    }
}


/**
 * ResponseBody.create replace
 */

//import okhttp3.MediaType.Companion.toMediaType
//import okhttp3.RequestBody.Companion.asRequestBody
//import okhttp3.RequestBody.Companion.toRequestBody
//import java.io.File
//        //String转RequestBody String、ByteArray、ByteString都可以用toRequestBody()
//        val stringBody ="body参数".toRequestBody("application/json;charset=utf-8".toMediaType())
//        val request: Request =Request
//            .Builder()
//            .post(stringBody)
//            .build()
//
//        //File转RequestBody
//        val file= File("")
//        val fileBody=file.asRequestBody("text/x-markdown; charset=utf-8".toMediaType())
//        val request = MultipartBody.Builder()
//            .addFormDataPart("file", file.name,fileBody)
//            .build()