package com.test.androidpractice.model

import com.google.gson.annotations.SerializedName

data class ContactAvailability(
    @SerializedName("contact_availability")
    var contact_availability:Boolean?
)