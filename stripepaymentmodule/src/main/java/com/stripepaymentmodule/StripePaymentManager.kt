package com.stripepaymentmodule

import android.app.Activity
import android.content.Context
import android.widget.Toast
import com.stripe.android.*
import com.stripe.android.model.CardParams
import com.stripe.android.model.Token
import com.stripe.android.view.CardInputWidget


class StripePaymentManager(
    var context: Context,
    var activity: Activity,
    var stripeView: CardInputWidget,
    var publishableKey: String
) {
    private var stripe: Stripe? = null
    private var idempotencyKey: String? = null
    private var stripeAccountId: String? = null

    init {
        PaymentConfiguration.init(
            context,
            publishableKey
        )
        stripe = Stripe(context, PaymentConfiguration.getInstance(context).publishableKey)
    }


    fun addCard() {
        var card = stripeView.cardParams

        if (card != null) {
            tokenizeCard(card)
        } else {

        }
    }

    private fun tokenizeCard(card: CardParams) {
        stripe!!.createCardToken(
            card, idempotencyKey, stripeAccountId,
            callback = object : ApiResultCallback<Token> {
                override fun onSuccess(result: Token) {
                }

                override fun onError(e: Exception) {
                    Toast.makeText(
                        context,
                        e.localizedMessage,
                        Toast.LENGTH_LONG
                    ).show();
                }
            })
    }
}


