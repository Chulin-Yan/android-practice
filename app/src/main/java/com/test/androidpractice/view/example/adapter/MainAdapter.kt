package com.test.androidpractice.view.example.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.test.androidpractice.R
import com.test.androidpractice.databinding.BreakfastItemStyle01Binding
import com.test.androidpractice.databinding.BreakfastItemStyle02Binding
import com.test.androidpractice.model.BreakfastItem
import com.test.androidpractice.utils.loadImage


/**
 * Example Adapter
 * Please copy me when you need create new Adapter
 */
class MainAdapter(
    private val context: Context,
    private val displayList: ArrayList<BreakfastItem?>
) :
    RecyclerView.Adapter<MainAdapter.ViewHolder>() {
    private var type: Int? = null

    /**
     * types
     */
    class ViewHolder(var dataBinding: ViewDataBinding) : RecyclerView.ViewHolder(dataBinding.root)
    companion object {
        const val TYPE_ONE = 0
        const val TYPE_TWO = 1
    }

    /**
     * select interface
     */
    private var onSelectClickedListener: OnSelectClickedListener? = null

    fun setOnSelectClickedListener(onSelectClickedListener: OnSelectClickedListener?) {
        this.onSelectClickedListener = onSelectClickedListener
    }

    interface OnSelectClickedListener {
        fun onSelectClicked(item: String)
    }

    /**
     * init layout for different type
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        type = when (viewType) {
            TYPE_ONE -> TYPE_ONE
            else -> TYPE_TWO
        }
        return when (viewType) {
            TYPE_ONE -> ViewHolder(
                DataBindingUtil.inflate<BreakfastItemStyle01Binding>(
                    inflater,
                    R.layout.breakfast_item_style_01,
                    parent,
                    false
                )
            )
            else -> ViewHolder(
                DataBindingUtil.inflate<BreakfastItemStyle02Binding>(
                    inflater,
                    R.layout.breakfast_item_style_02,
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemCount(): Int = displayList.size

    /**
     * select type, must have default type
     */
    override fun getItemViewType(position: Int): Int {
        if (displayList[position]?.brand == null ) {
            type = TYPE_ONE
            return TYPE_ONE
        } else {
            type = TYPE_TWO
            return TYPE_TWO
        }
    }

    /**
     * edit action for item
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = displayList[position]
        item?.let {
            when (type) {
                TYPE_ONE -> {
                    val binding = holder.dataBinding as BreakfastItemStyle01Binding
                    binding.breakfastItem.elevation = 6.0F
                    
                    item.description?.let { binding.breakfastDescription.text = it.toUpperCase() }
                    item.imageName?.let {
                        loadImage(binding.breakfastImage, it, null, context)
                    }
                    item.prep_time?.let { binding.estimatedTime.text = it.toString() }
                    if(item.favorite == true){
                        binding.likeImage.setBackgroundResource(R.drawable.ic_heart_shape_red)
                    }else{
                        binding.likeImage.setBackgroundResource(R.drawable.ic_heart_shape_border_red)
                    }

                    item.calorie?.let { binding.popularity.text = it.toString() }
                    item.serving_size?.let { binding.recommendations.text = it.toString() }

                    binding.breakfastItem.setOnClickListener {
                        onSelectClickedListener?.onSelectClicked(position.toString())
                    }
                    binding.likeImage.setOnClickListener {
                        onSelectClickedListener?.onSelectClicked(position.toString() + "toggle")
                    }
                }
                else -> {
                    val binding = holder.dataBinding as BreakfastItemStyle02Binding
                    binding.breakfastItem.elevation = 6.0F
                    item.description?.let { binding.breakfastDescription.text = it.toUpperCase() }
                    item.imageName?.let {
                        loadImage(binding.breakfastImage, it, null, context)
                    }
                    if(item.favorite == true){
                        binding.likeImage.setBackgroundResource(R.drawable.ic_heart_shape_red)
                    }else{
                        binding.likeImage.setBackgroundResource(R.drawable.ic_heart_shape_border_red)
                    }

                    binding.breakfastItem.setOnClickListener {
                            onSelectClickedListener?.onSelectClicked(position.toString())
                    }
                    binding.likeImage.setOnClickListener {
                            onSelectClickedListener?.onSelectClicked(position.toString() + "toggle")
                    }
                }
            }
        }
    }
}