package com.test.androidpractice.components

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.LifecycleOwner
import com.test.androidpractice.R
import com.test.androidpractice.databinding.ItemExampleListItemBinding

class ExampleCustomView(context: Context, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {
    private val binding =
        ItemExampleListItemBinding.inflate(LayoutInflater.from(context), this, true).apply {
            lifecycleOwner = context as? LifecycleOwner
        }

    //interface
    //select layout
    private var onSelectClickedListener: OnSelectClickedListener? = null

    fun setOnSelectClickedListener(onSelectClickedListener: OnSelectClickedListener?) {
        this.onSelectClickedListener = onSelectClickedListener
    }

    interface OnSelectClickedListener {
        fun onSelectClicked() //add return message
    }

    init {
        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.ExampleCustomView)
            try {
                //set styleable variables
            } finally {
                a.recycle()
            }
        }
        setCallBackEvents()
    }

    private fun setCallBackEvents() {
        binding.text.setOnClickListener {
            onSelectClickedListener?.onSelectClicked()
        }
    }
}