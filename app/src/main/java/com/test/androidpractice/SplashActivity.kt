package com.test.androidpractice

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.test.androidpractice.base.Global.Companion.testInfo
import com.test.androidpractice.databinding.ActivitySplashBinding
import com.test.androidpractice.utils.InAppUpdateUtils
import com.test.androidpractice.utils.NetworkChecker
import com.test.androidpractice.utils.SharedPreferenceUtils
import com.test.androidpractice.view.example.activity.LoginActivity
import com.test.androidpractice.view.example.activity.MyCommitmentActivity
import com.test.androidpractice.view.example.activity.VerificationActivity

class SplashActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySplashBinding
    private lateinit var inAppUpdateUtils: InAppUpdateUtils
    private val MY_REQUEST_CODE = 323


    /**
     * activity starter
     */
    companion object {
        fun start(context: Context) {
            val intent = Intent(context, SplashActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //AccountManager.init(this)
       SharedPreferenceUtils.initSP(this, "headers")
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)

        if (NetworkChecker.isConnective(this)) {
            inAppUpdateUtils = InAppUpdateUtils(
                this,
                binding.layoutBase,
                MY_REQUEST_CODE,
                null,
                BuildConfig.VERSION_NAME
            )
            inAppUpdateUtils.checkUpdate()
            inAppUpdateUtils.setOnStartAppListener(object : InAppUpdateUtils.OnStartAppListener {
                override fun onStartApp(testInfo: String) {
                    Log.d("update: ", "check update information: $testInfo")
                    startApp()
                }
            })
        } else {
            NetworkChecker.askForInternet(this, "This App need Internet for use.", "Network ")
        }
    }

    private fun startApp() {
        //todo start your app here
        binding.clickButton.setOnClickListener{
           LoginActivity.start(this)
        }
        binding.btnToCommitment.setOnClickListener{
            MyCommitmentActivity.start(this)
        }
    }

    override fun onBackPressed() {
        //do nothing
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MY_REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                testInfo += " update flow failed code: $requestCode"
                startApp()
            }
        }
    }
}