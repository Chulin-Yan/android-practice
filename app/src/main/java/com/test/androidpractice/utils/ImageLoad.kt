package com.test.androidpractice.utils

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

/**
 * this is public fun for loading image by glide
 */

fun loadImage(view: ImageView, uri: String?, placeholder: Int?, context: Context) {
    if (placeholder != null) {
        Glide.with(context)
            .setDefaultRequestOptions(RequestOptions().placeholder(placeholder))
            .load(uri)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .into(view)
    } else {
        Glide.with(context)
            .load(uri)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .into(view)
    }
}