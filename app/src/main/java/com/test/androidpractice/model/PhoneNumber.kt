package com.test.androidpractice.model

import com.google.gson.annotations.SerializedName

data class PhoneNumber(
    @SerializedName("phone")
    val phone:String?
)