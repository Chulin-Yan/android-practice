package com.test.androidpractice

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.google.gson.Gson
import com.test.androidpractice.model.User

class AccountManager {

    companion object{
        lateinit var sSharedPreferences:SharedPreferences
        var userToken: User.Token?= null
        fun init(context: Context) {
            Log.d("account","startinit")
            sSharedPreferences = context.getSharedPreferences(
                "user_account",
                Context.MODE_PRIVATE
            )
            Log.d("acc","initiated")
        }
        fun saveToken(token: User.Token) {
            val gson = Gson()
            val tokenStr = gson.toJson(token)
            sSharedPreferences.edit().putString("token_info", tokenStr)?.apply()
        }

        fun getToken(): User.Token {
            if(userToken != null){
                return  userToken!!
            }

            val tokenStr: String? =
                sSharedPreferences?.getString(
                    "token_info",
                    null
                )
            var token = User.Token()
            try {
                token = Gson().fromJson(tokenStr, User.Token::class.java)
                if(token.accessToken.isNullOrBlank()){
                   Log.d("access-toke","is null")
                }

            } catch (e: Exception) {
            }
            return token
        }
    }


}