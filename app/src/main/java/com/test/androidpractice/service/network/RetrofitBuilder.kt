package com.test.androidpractice.service.network

import com.test.androidpractice.BuildConfig
import com.test.androidpractice.service.network.interceptors.AuthorizationInterceptor
import com.test.androidpractice.service.network.interceptors.BadConnectInterceptor
import com.test.androidpractice.service.network.interceptors.HeaderInterceptor
import com.test.androidpractice.service.network.interceptors.LoggingInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.net.ssl.X509TrustManager


object RetrofitBuilder {

    private const val BASE_URL = BuildConfig.ROOT_URL
    private var timeout: Long = 20


    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(createClient())
            .build()
    }

    private fun createClient(): OkHttpClient {
        val client: OkHttpClient.Builder = OkHttpClient.Builder()
//            .certificatePinner(SSLCertificateHelper.certificatePinner())
//            .sslSocketFactory(
//                SSLCertificateHelper.getSSLSocketFactory(),
//                SSLCertificateHelper.getTrustManager() as X509TrustManager
//            )
 //           .hostnameVerifier(SSLCertificateHelper.hostNameVerifier(BASE_URL))
            .addInterceptor(HeaderInterceptor())
            .addInterceptor(AuthorizationInterceptor())

            .connectTimeout(timeout, TimeUnit.SECONDS)
            .readTimeout(timeout, TimeUnit.SECONDS)
            .writeTimeout(timeout, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)

        if (BuildConfig.DEBUG) {
            client.addInterceptor(LoggingInterceptor())
            client.addInterceptor(BadConnectInterceptor())
        } else {
            client.addInterceptor(BadConnectInterceptor())
        }

        return client.build()
    }

    val apiService: Api = getRetrofit().create(Api::class.java)
}