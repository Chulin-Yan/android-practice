package com.test.androidpractice.utils

import android.content.Context
import java.io.IOException
import java.nio.charset.StandardCharsets


class ReadAssetUtil {

    companion object {
        @JvmStatic
        fun loadJSONFromAsset(context: Context, filePath: String): String? {
            var json: String? = null
            json = try {
                val inputStream = context.assets.open(filePath!!)
                val size = inputStream.available()
                val buffer = ByteArray(size)
                inputStream.read(buffer)
                inputStream.close()
                String(buffer, StandardCharsets.UTF_8)
            } catch (ex: IOException) {
                ex.printStackTrace()
                return null
            }
            return json
        }
    }
}