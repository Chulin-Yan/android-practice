package com.test.androidpractice.service.network

import android.util.Log
import com.test.androidpractice.BuildConfig
import com.test.androidpractice.base.AppController
import okhttp3.CertificatePinner
import java.security.KeyStore
import java.security.cert.Certificate
import java.security.cert.CertificateFactory
import javax.net.ssl.*

class SSLCertificateHelper {

    companion object {


        /**
         * CertificatePining CA
         * please set pattern and pin in gradle.properties
         *
         * in order to use this feature, the app must be acquired from Play
         * otherwise get error ERROR_APP_NOT_OWNED
         *
         * don't forget register in createClient()
         */
        fun getCertificatePinner(): CertificatePinner {
            return CertificatePinner.Builder()
                .add(BuildConfig.CERTIFICATE_PATTERN, BuildConfig.CERTIFICATE_PIN)
                .build()
        }


        /**
         *  CertificatePining Local
         *  Please call  .sslSocketFactory(getSSLSocketFactory(), getTrustManager() as X509TrustManager)
         *  when build client and change Certification file path
         */
        private fun loadCertificate(): Certificate {
            return AppController.applicationContext().assets
                .open("fake_certificate.cer").use { input ->
                    CertificateFactory.getInstance("X.509").generateCertificate(input)
                }
        }

        fun getTrustManager(): TrustManager? {
            val keyStore = KeyStore.getInstance(KeyStore.getDefaultType())

            //certificate must be android supported type e.g. BKS, use Keystore.Explorer software to change your jks
            //or use .cer file and following code instead inputStream

        keyStore.load(null)
        keyStore.setCertificateEntry("123456", loadCertificate())

//            val inputStream: InputStream =
//                AppController.applicationContext().assets.open("my_server.jks")
//            inputStream.use { inputStream ->
//                keyStore.load(inputStream, "123456".toCharArray())
//            }


            return TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm()).run {
                init(keyStore)
                trustManagers[0]
            }
        }

        fun hostNameVerifier(hostName: String): HostnameVerifier {
             return HostnameVerifier { name, session ->
                if (name == hostName) {
                    Log.d("check: ", "trust manager working $name")
                    true
                } else {
                    Log.d("check: ", "Wrong trust manager ")
                    false
                }
            }
        }

        fun getSSLSocketFactory(): SSLSocketFactory {
            val sslContext = SSLContext.getInstance("SSL").apply {
                init(null, arrayOf(getTrustManager()), java.security.SecureRandom())
            }
            return sslContext.socketFactory
        }
    }
}