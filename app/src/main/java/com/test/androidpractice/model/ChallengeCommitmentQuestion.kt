package com.test.androidpractice.model

import com.google.gson.annotations.SerializedName

data class ChallengeCommitmentQuestion(
    @SerializedName("black_area")
    var black_area: ArrayList<ChallengeCommitmentQuestionData?>?,
    @SerializedName("white_area")
    var white_area: ArrayList<ChallengeCommitmentQuestionData?>?,
)
