package com.test.androidpractice.service.network

import com.test.androidpractice.model.*
import retrofit2.Response
import retrofit2.http.*
import kotlin.collections.ArrayList


interface Api {

    @GET("/api/v1/get_user")
    suspend fun getUser(): Response<UserOrigin>


    @GET("/api/v1/challenge/recipes")
    suspend fun getRecipes(
        @Query("recipe_category_id") recipeCategoryId: String,
        @Query("keywords") keyword: String,
        @Query("searching_time") searchingTime: String,
        @Query("page") page: String,
        @Query("meal_plan_type") mealPlanType: String
    ): Response<ArrayList<BreakfastItem?>?>

    @GET("/api/v1/challenge/commitment/new")
    suspend fun getChallengeCommitmentQuestion(): Response<ChallengeCommitmentQuestion?>

    @POST("/api/v1/members/send_sms_code")
    suspend fun sendVerificationCode(@Query("phone") phoneNumber: String): Response<PhoneNumber>

    @POST("/api/v1/auth/sign_in")
    suspend fun signIn(@Query("phone") phone: String, @Query("code") code: String): Response<User>


//    @POST("/api/v1/survey-entries")
//    suspend fun getQuestionList(): Response<DataQuestionList>
//
//    @POST("/api/v1/feedbacks/results")
//    suspend fun getFeedbackURL(@Body feedback:FeedBack): Response<FeedbackURL>
//
//    @POST("/api/v1/finished-questions")
//    suspend fun finishedQuestions(@Body param: DataUploadQuestion): Response<DataQuestion>
//
//    @GET("/api/v1/grants")
//    suspend fun getGrantResults(@Query("survey_entry_id") surveyEntryID: Int): Response<DataGrantList>
//
//    @GET("/api/v1/contacts/availability")
//    suspend fun getContactsAvailability(): Response<ContactAvailability>
//
//    @GET("/api/v1/questions/{question_id}")
//    suspend fun refreshSurvey(
//        @Path("question_id") questionID: Int,
//        @Query("survey_entry_id") surveyEntryID: Int,
//        @Query("pre_question_id") preQuestionID: Int?
//    ): Response<DataQuestion>
}