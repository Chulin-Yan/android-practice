package com.test.androidpractice.view.example.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.test.androidpractice.R
import com.test.androidpractice.base.BaseActivity
import com.test.androidpractice.databinding.ActivityLoginBinding
import com.test.androidpractice.service.network.utils.Status
import com.test.androidpractice.viewmodel.LoginViewModel


/**
 * this is a example for activity with full general method
 * this also a good start for most project main page
 */
class LoginActivity : BaseActivity() {
    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: LoginViewModel

    /**
     * activity starter
     */
    companion object {
        fun start(context: Context,) {
            val intent = Intent(context, LoginActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        //data binding with view model
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        initView()
    }


    private fun callAPIsForLogin() {
        showLoading()
        viewModel.loadData(this, true).observe(this, Observer { it ->
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        Log.d("status",resource.status.toString())
                        hideLoading()
                        resource.data?.let {
                            Log.d("phone",it.phone.toString())
                        VerificationActivity.start(this, it.phone.toString())
                        }
                    }

                    Status.ERROR -> {
                        hideLoading()
                        Log.d("status",resource.status.toString())
                    }

                    Status.LOADING -> {
                        Log.d("status",resource.status.toString())
                    }
                }
            }
        })
    }


    private fun clickEvent() {
        //todo add click event
    }

    private fun initView() {
        binding.loginButton.setOnClickListener {
            if (TextUtils.isEmpty(viewModel.mobilePhoneNumber.value)) {
                Toast.makeText(
                    this@LoginActivity,
                    "Please input mobile phone number",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            } else {
                //callAPIsForLogin()
                viewModel.mobilePhoneNumber.value?.let { it1 ->
                    VerificationActivity.start(this,
                        it1
                    )
                }
            }
        }

    }

    override fun onResume() {
        super.onResume()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


}