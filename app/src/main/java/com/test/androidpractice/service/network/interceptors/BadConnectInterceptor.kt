package com.test.androidpractice.service.network.interceptors

import android.annotation.SuppressLint
import android.util.Log
import okhttp3.*

class BadConnectInterceptor() : Interceptor {
    @SuppressLint("LogNotTimber")
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        var response: Response = chain.proceed(request)
        var tryCount = 0
        var responseOK = response.isSuccessful
        var responseBodyString = ""
        while (!responseOK && tryCount < 5) {
            try {
                responseOK = response.isSuccessful
//                if(!responseOK){
//                    Log.d("check: ", "Request is not successful - $tryCount")
//                    responseBodyString="Network time out."
//                }
            } catch (e: Exception) {
                Log.d("check: ", "Request is not successful - $tryCount")
                responseBodyString = "Network time out."
            } finally {
                response.close()
                tryCount++
                response = chain.proceed(request)
            }
        }

//        val stringBody =responseBodyString.toRequestBody("application/json;charset=utf-8".toMediaType())
//        val request2: Request =Request
//            .Builder()
//            .post(stringBody)
//            .build()
//        return  chain.proceed(request2)
//
//        return Response.Builder()
//                .code(response.code)
//                .body(response.body)
//                .protocol(Protocol.HTTP_2)
//                .message("Network time out.")
//                .request(chain.request())
//                .build()
        return response
    }


}


/**
 * ResponseBody.create replace
 */

//import okhttp3.MediaType.Companion.toMediaType
//import okhttp3.RequestBody.Companion.asRequestBody
//import okhttp3.RequestBody.Companion.toRequestBody
//import java.io.File
//        //String转RequestBody String、ByteArray、ByteString都可以用toRequestBody()
//        val stringBody ="body参数".toRequestBody("application/json;charset=utf-8".toMediaType())
//        val request: Request =Request
//            .Builder()
//            .post(stringBody)
//            .build()
//
//        //File转RequestBody
//        val file= File("")
//        val fileBody=file.asRequestBody("text/x-markdown; charset=utf-8".toMediaType())
//        val request = MultipartBody.Builder()
//            .addFormDataPart("file", file.name,fileBody)
//            .build()