package com.test.androidpractice.base

import android.content.Context

class Global {

    companion object{
        /**
         * test/debug data
         */
        const val CHECK = "check: "
        var testInfo=""

        /**
         * web view
         */
        val URL_DATA="url_data"


        /**
         * Global Data
         */
        lateinit var applicationContext: Context


        fun init(context:Context){
            applicationContext=context
        }

        fun getDeviceId(): String {
//            return if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//                val telephonyManager:TelephonyManager = applicationContext.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
//                telephonyManager.meid
//            } else {
//                Settings.Secure.getString(
//                    applicationContext.contentResolver, Settings.Secure.ANDROID_ID
//            }
            return ""
        }

//        @SuppressLint("LogNotTimber")
//        fun firebaseEvent(context: Context, id: String, codeName: String?, surveyID: String?) {
//            if (BuildConfig.ENABLE_ANALYTICS) {
//                val bundle = Bundle()
//                codeName?.let { bundle.putString("code_name", it) }
//                surveyID?.let { bundle.putString("survey_id", it) }
//                bundle.putString("created_at", dateToTimestamp().toString())
//                val firebaseAnalytics = FirebaseAnalytics.getInstance(context)
//                firebaseAnalytics.logEvent(id, bundle)
//                Log.d(CHECK, "Firebase:Sending event log -- id: $id, code name: $codeName, survey id: $surveyID, time: ${dateToTimestamp()}")
//            }
//        }
//
//        @SuppressLint("LogNotTimber")
//        fun facebookEvent(context: Context, id: String, codeName: String?, surveyID: String?) {
//            if (BuildConfig.ENABLE_ANALYTICS) {
//                val logger = AppEventsLogger.newLogger(context)
//                val bundle = Bundle()
//                codeName?.let { bundle.putString("code_name", it) }
//                surveyID?.let { bundle.putString("survey_id", it) }
//                bundle.putString("created_at", dateToTimestamp().toString())
//                logger.logEvent(id, bundle)
//                Log.d(CHECK, "Facebook:Sending event log -- id: $id, code name: $codeName, survey id: $surveyID, time: ${dateToTimestamp()}")
//            }
//        }
    }
}