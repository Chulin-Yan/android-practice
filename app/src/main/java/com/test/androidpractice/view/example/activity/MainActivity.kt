package com.test.androidpractice.view.example.activity

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.test.androidpractice.R
import com.test.androidpractice.SplashActivity
import com.test.androidpractice.base.BaseActivity
import com.test.androidpractice.databinding.ActivityMainBinding
import com.test.androidpractice.model.BreakfastItem
import com.test.androidpractice.service.network.utils.Status
import com.test.androidpractice.service.network.utils.callAPI
import com.test.androidpractice.utils.ReadAssetUtil
import com.test.androidpractice.view.example.adapter.MainAdapter
import com.test.androidpractice.viewmodel.MainViewModel


/**
 * this is a example for activity with full general method
 * this also a good start for most project main page
 */
class MainActivity : BaseActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel
    private lateinit var mAdapter: MainAdapter
    private var displayList: ArrayList<BreakfastItem?> = arrayListOf()
    private var isFirstTime = false

    /**
     * activity starter
     */
    companion object {
        fun start(context: Context, isFirstTime: Boolean) {
            val intent = Intent(context, MainActivity::class.java)
            intent.putExtra("isFirstTime", isFirstTime)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // set status bar color
        val window: Window = window
        window.setStatusBarColor(Color.parseColor("#2A303F"))
        if (Build.VERSION.SDK_INT < 30) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        } else {
            window.setDecorFitsSystemWindows(false)
//            val controller = getWindow().insetsController
//            controller?.setSystemBarsAppearance(
//            )
        }
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        //data binding with view model
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        //Optional
        isFirstTime = intent.getBooleanExtra("isFirstTime", false)
        //window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

        //set adapter
        mAdapter = MainAdapter(this, displayList)
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        binding.recyclerview.apply {
            layoutManager = linearLayoutManager
            adapter = mAdapter
        }
        //set backwards option
        binding.backward.setOnClickListener{
            SplashActivity.start(this)
        }
        //general function
        //initView()

        clickEvent()
        callAPIsForBreakfastItems()

        //simple call
        callAPI()
    }


    private fun callAPIsForBreakfastItems() {
 //       updateBreakfastItem(readTestInput())
        showLoading()
        viewModel.loadData(this, true).observe(this, Observer { it ->
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        hideLoading()
                        resource.data?.let {
                            updateBreakfastItem(it)
                        }
                    }

                    Status.ERROR -> {
                        hideLoading()
                    }

                    Status.LOADING -> {

                    }
                }
            }
        })
    }


    private fun updateBreakfastItem(breakfastItemList: ArrayList<BreakfastItem?>) {
        //todo update user
        displayList.clear()
        displayList.addAll(breakfastItemList)
        mAdapter.notifyDataSetChanged()
    }

    private fun clickEvent() {
        //todo add click event
        mAdapter.setOnSelectClickedListener(object : MainAdapter.OnSelectClickedListener {
            override fun onSelectClicked(item: String) {
                Log.d("clickEvent", item)
                if (item.length > 1) {
                    var itemPosition = item.substring(0, 1).toInt()
                    if (displayList[itemPosition]?.favorite == true) {
                        displayList[itemPosition]?.favorite = false
                        mAdapter.notifyDataSetChanged()

                    } else {
                        displayList[itemPosition]?.favorite = true
                        mAdapter.notifyDataSetChanged()
                    }
                } else {
                    jumpToDetail()

                }
            }
        })

    }
    private fun jumpToDetail(){
        BreakfastDetailActivity.start(this)
    }
    private fun initView() {
        //todo init view
        //get/set live data programmatically
        val anotherName = "John Doe"
        viewModel.currentName.value = anotherName
        //observe live data programmatically
        viewModel.currentName.observe(this, {
            //binding.tvText2.text = it
        })
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

//
//    private fun logEvent(codeName: String) {
//        firebaseEvent(this, DASHBOARD_EVENT, codeName, null)
//        facebookEvent(this, DASHBOARD_EVENT, codeName, null)
//    }


    /**
     * testing json data
     */
    private fun readTestInput(): ArrayList<BreakfastItem?> {
        val listType = object : TypeToken<ArrayList<BreakfastItem>>() {}.type
        val jsonStr: String? = ReadAssetUtil.loadJSONFromAsset(this, "json/mockBreakfastItems.json")
        return Gson().fromJson(jsonStr, listType)
    }
}