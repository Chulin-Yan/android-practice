package com.test.androidpractice.view.example.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.test.androidpractice.R
import com.test.androidpractice.base.BaseFragment
import com.test.androidpractice.databinding.FragmentExampleBinding
import com.test.androidpractice.model.UserOrigin
import com.test.androidpractice.service.network.utils.Status
import com.test.androidpractice.viewmodel.MainViewModel

/**
 * Example Fragment
 * Please copy me when you need create new fragment
 */

class ExampleFragment:BaseFragment() {
    private lateinit var binding: FragmentExampleBinding
    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_example, container, false)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnSend.setOnClickListener {
            callAPIForLogin()
        }
        binding.refreshLayout.setOnRefreshListener {
            callAPIForLogin()
        }
    }

    private fun callAPIForLogin() {
        viewModel.loadData(requireContext(),true).observe(viewLifecycleOwner, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        binding.refreshLayout.isRefreshing = false
                        //do something here
//                        resource.data?.let { it1 -> updateUser(it1) }
                    }
                    Status.ERROR -> {
                        binding.refreshLayout.isRefreshing = false
                    }
                    Status.LOADING -> {
                    }
                }
            }
        })
    }

    private fun updateUser(userOrigin: UserOrigin) {

    }
}