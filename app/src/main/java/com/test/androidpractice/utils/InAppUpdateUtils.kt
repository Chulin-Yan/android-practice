package com.test.androidpractice.utils

import android.app.Activity
import android.content.IntentSender
import android.view.View
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallState
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability

class InAppUpdateUtils(val activity: Activity, val layoutBase: View, val MY_REQUEST_CODE: Int, var minVersion: String?, var currentVersion: String?) {
    private var appUpdateManager: AppUpdateManager? = null
    private var testInfo = "No information"
    var snackbar: Snackbar? = null
    var snackBarText: TextView? = null
    private var updatedListener = InstallStateUpdatedListener { state: InstallState ->
        if (state.installStatus() == InstallStatus.DOWNLOADED) {
            snackBarText!!.text = "Download Complete"
            snackbar!!.setAction("Install") { v: View? -> appUpdateManager!!.completeUpdate() }
            snackbar!!.show()
        }
        if (state.installStatus() == InstallStatus.DOWNLOADING) {
            snackBarText!!.text = "Downloading..."
            val percent = state.bytesDownloaded() * 100 / state.totalBytesToDownload().toFloat()
            snackbar!!.setAction(String.format("%.0f%%", percent)) { v: View? -> }
            snackbar!!.show()
        }
    }

    //interface
    private var onStartAppListener: OnStartAppListener? = null

    fun setOnStartAppListener(onStartAppListener: OnStartAppListener?) {
        this.onStartAppListener = onStartAppListener
    }

    interface OnStartAppListener {
        fun onStartApp(testInfo: String)
    }

    fun checkUpdate() {
        snackbar = Snackbar.make(layoutBase, "", Snackbar.LENGTH_INDEFINITE)
        snackBarText = snackbar!!.view.findViewById(com.google.android.material.R.id.snackbar_text)
        appUpdateManager = AppUpdateManagerFactory.create(activity)
        appUpdateManager!!.registerListener(updatedListener)
        val appUpdateInfoTask = appUpdateManager!!.appUpdateInfo
        //No update available from google play store
        appUpdateInfoTask.addOnFailureListener { e: Exception? ->
            testInfo = "Update task fail:$e"
            onStartAppListener?.onStartApp(testInfo)
        }
        //At lease one update available from google play store
        appUpdateInfoTask.addOnSuccessListener { result: AppUpdateInfo ->
            testInfo = "Update task success: $result"
            if (result.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                testInfo = "Update task available"
                try {
                    appUpdateManager!!.startUpdateFlowForResult(result, AppUpdateType.FLEXIBLE, activity, MY_REQUEST_CODE)
                    testInfo = "Update task start update"
                } catch (e: IntentSender.SendIntentException) {
                    testInfo = "Update task cant start: $e"
                    e.printStackTrace()
                }

                if (minVersion != null && currentVersion != null) {
                    val minVer = Integer.parseInt(minVersion!!.replace(".", ""))
                    val currentVer = Integer.parseInt(currentVersion!!.replace(".", ""));
                    try {
                        if (currentVer >= minVer) {
                            appUpdateManager!!.startUpdateFlowForResult(result, AppUpdateType.FLEXIBLE, activity, MY_REQUEST_CODE);
                        } else {
                            appUpdateManager!!.startUpdateFlowForResult(result, AppUpdateType.IMMEDIATE, activity, MY_REQUEST_CODE);
                        }
                    } catch (e: IntentSender.SendIntentException) {
                        e.printStackTrace()
                    }
                }
            } else {
                onStartAppListener?.onStartApp(testInfo)
                testInfo = "Update task not available"
            }
        }
    }

    /**
     * add onActivityResult in your activity to catch RESULT
     */
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode == MY_REQUEST_CODE) {
//            if (resultCode != RESULT_OK) {
//                Global.testInfo += " update flow failed code: $requestCode"
//                startApp()
//            }
//        }
//    }

}